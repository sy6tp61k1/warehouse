program Quest;

uses
  crt, read;

const
  Up = #38;
  Down = #40;
  Days = 30;

label marker, marker2, marker3;


function FindCounter(Tag: String; Counter: Integer): Integer;
begin
  if (Tag = '1') or (Tag = '1112') or (Tag = '11111') or (Tag = '11112') or (Tag = '11212') or (Tag = '1121211') or (Tag = '1121212') or (Tag = '112221') or (Tag = '1122�21') or (Tag = '11222111') or (Tag = '1122�2111') or (Tag = '11222112') or (Tag = '1122�2112') or (Tag = '11222211') or (Tag = '1122�2211') or (Tag = '11222212') or (Tag = '1122�2212') or (Tag = '11222221') or (Tag = '1122�2221') or (Tag = '11222222') or (Tag = '1122�2222') or (Tag = '11221') or (Tag = '1122�1') or (Tag = '11211') or (Tag = '1122222') then Counter := 1;
  if (Tag = '11') or (Tag = '111') or (Tag = '112') or (Tag = '1111') or (Tag = '1121') or (Tag = '1122') or (Tag = '1122�') or (Tag = '11222') or (Tag = '1122�2') or (Tag = '112121') or (Tag = '1122211') or (Tag = '1122221') or (Tag = '1122a211') or (Tag = '1122�221') or (Tag = '1122�222')  then Counter := 2;
  FindCounter := Counter
end;


function FindTag(Tag: String; Posicion: Integer ): String;
begin
  case Posicion of
    0: Tag := Tag + '1';
    1: Tag := Tag + '2';
  end;
  FindTag := Tag;
end;


var
  Key: char;
  Posicion, Counter, DayCounter, i, PostponementOfExit: integer;
  
  Tag: string;

begin
  PostponementOfExit := 0;
  DayCounter := 0;
  marker:
  clrscr;
  Tag := '1';
  
  while DayCounter <> Days  do
  begin
    
    
    Key := '0';
    Posicion := 0;
    
    
    clrscr;
    find(Tag);
    gotoXY(70, 23);
    write('����', DayCounter+1);
    gotoXY(1, 2);
    
    writetxt;
    
    gotoXY(2, 17);
    
    write('*');
    Counter := FindCounter(Tag, Counter);
    
    for i := 1 to Counter do
    begin
      
      gotoXY(4, 16 + i);
      
      writeot;
    end;
    
    
    while(Key <> #13) do
    begin
      Key := ReadKey;
      if Key = #0 then Key := readkey;
      case Key of
        Up:
          begin
            if Posicion > 0 then
            begin
              textcolor(0); gotoXY(2, 17 + Posicion);   write('*'); Posicion := Posicion - 1;textcolor(7); gotoXY(2, 17 + Posicion);   write('*');
            end;
          end;
        
        Down:
          begin
            if Posicion < Counter - 1 then
            begin
              textcolor(0); gotoXY(2, 17 + Posicion);   write('*');  Posicion := Posicion + 1; textcolor(7); gotoXY(2, 17 + Posicion);   write('*');
            end;
          end;
      End;
      
    end;
    Tag := FindTag(Tag, Posicion);
    
    if Tag = '1122' then PostponementOfExit := PostponementOfExit + 1;
    if (PostponementOfExit >= 3) then 
    begin
    if (Tag = '1122') and (PostponementOfExit mod 3 =0) then  Tag := Tag + '�';
    if (Tag = '11221') and not(PostponementOfExit mod 3 =0) then  Tag :='1122�1';
    if (Tag = '11222') and not(PostponementOfExit mod 3 =0) then  Tag :='1122�2';
   
    end;
    if (Tag = '11121') or (Tag = '111111') or (Tag = '111121') or (Tag = '112111') or (Tag = '112211') or (Tag = '1122�11') or (Tag = '11212111') or (Tag = '11212121') or (Tag = '112221111') or (Tag = '1122�21111') or (Tag = '112221121') or (Tag = '1122�21121') or  (Tag = '112222111')  or  (Tag = '1122�22111') or  (Tag = '112222121') or  (Tag = '1122�22121') or  (Tag = '112222121') or (Tag = '1122�22121') or  (Tag = '112222221') or  (Tag = '1122�22221') or (Tag = '1122222211') or (Tag = '1122�222211') or (Tag = '112222211') or (Tag = '1122�22211') or (Tag = '1122�22221') then
    begin
      DayCounter := DayCounter + 1;
      if (Tag = '1122�22221') then
        goto marker2
      else goto marker;
    end;
  end;
  
  gotoXY(32, 3);
  writeln('�����, �� ���������');
  goto marker3;
  
  marker2:
  clrscr;
  gotoXY(8, 3);
  
  writeln('������ ���������.������� �����! "������" - ������� ��� ���� ���. ');
  marker3:
  Key:=readkey;
  close(TextOfFile);
  
  
end.