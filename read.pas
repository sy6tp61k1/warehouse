unit read;

interface

var
  
  Counter: string[255];
  TextOfFile: text;
  i: integer;

procedure find(Tag: string);
procedure writetxt;
procedure writeot;

implementation

procedure find(Tag: string);
begin
  reset(TextOfFile);
  while Counter <> Tag do
    readln(TextOfFile, Counter);
  
end;

procedure writetxt;
begin
  i := 0;
  readln(TextOfFile, Counter);
  while i = 0 do
  begin
    writeln(Counter);
    readln(TextOfFile, Counter);
    if Counter = '$' then i := i + 1;
  end;
end;

procedure writeot;
begin
  readln(TextOfFile, Counter);
  writeln(Counter);
end;

begin
  assign(TextOfFile, 'output.txt');
  reset(TextOfFile);
End.